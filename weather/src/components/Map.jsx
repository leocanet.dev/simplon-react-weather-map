import "../styles/Map.css";
import Json from "../simplon.json";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { useState } from "react";
import Info from "./Info";
import Localisation from "./Localisation";
import Icon from "./IconFab";

function App() {
  const [data, setData] = useState(0);
  // useState initialiser a null + elem? dans PopUp
  const [elem, setWeather] = useState(null);

  function changerIndex(index) {
    setData(index);
  }

  function getWeather(lat, long) {
    fetch(
      "https://api.openweathermap.org/data/2.5/weather?lat=" +
        lat +
        "&lon=" +
        long +
        "&units=metric&lang=fr&appid=95a40b54331210f1bcdbd1d4d2597ebf"
    )
      .then((response) => response.json())
      .then((promise) => {
        let jsonTab = promise;
        console.log(jsonTab);
        setWeather(jsonTab);
      })
      .catch((err) => {
        console.log(err);
      });
  }

  function getIconWeather(name) {
    if (name === undefined) {
      return "";
    } else {
      return "http://openweathermap.org/img/wn/" + name + "@2x.png";
    }
  }

  return (
    <div className="App" id="map">
      <div className="box-info">
        <div className="box-fabrique">
          <Info num={data}></Info>
        </div>
      </div>

      <MapContainer center={[43.505, 2.35]} zoom={7} scrollWheelZoom>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://api.maptiler.com/maps/topo/{z}/{x}/{y}.png?key=AwcJYM3GbEe5c0oZXkKZ"
        />
        <Localisation />
        {Json.map((fab, index) => {
          return (
            <Marker
              icon={Icon}
              position={[fab.latitude, fab.longitude]}
              key={index}
              eventHandlers={{
                click: () => {
                  changerIndex(index);
                  getWeather(fab.latitude, fab.longitude);
                },
              }}
            >
              <Popup>
                <h3>{fab.nom}</h3>

                <p className="tempe" style={{ margin: 0 + "rem" }}>
                  <img
                    src={getIconWeather(elem?.weather[0].icon)}
                    alt="icon-weather"
                    style={{ height: "50px", width: "50px", marginRight: "5px" }}
                  />
                  {Math.round(elem?.main.temp)}°C
                </p>
                <div className="box_one">
                  <p className="ressent" style={{ margin: 0 + "rem" }}>
                    Ressentie: {Math.round(elem?.main.feels_like)}°C
                  </p>
                  <p
                    className="descrip"
                    style={{ margin: 0 + "rem", textTransform: "capitalize" }}
                  >
                    {elem?.weather[0].description}
                  </p>

                  <p className="windkm" style={{ margin: 0 + "rem" }}>
                    {Math.round(elem?.wind.speed * 3.6)} km/h de vent
                  </p>
                  <p className="pressure" style={{ margin: 0 + "rem" }}>
                    {Math.round(elem?.main.pressure)} hPa
                  </p>

                  <p className="humidity" style={{ margin: 0 + "rem" }}>
                    Humidité: {Math.round(elem?.main.humidity)}%
                  </p>
                  <p className="visible" style={{ margin: 0 + "rem" }}>
                    Visibilité: {elem?.visibility / 1000} Km
                  </p>
                </div>
              </Popup>
            </Marker>
          );
        })}
      </MapContainer>
    </div>
  );
}

export default App;

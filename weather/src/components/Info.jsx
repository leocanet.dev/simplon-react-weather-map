import Json from "../simplon.json";

function Info(props) {
  return (
    <div>
      <h2>{Json[props.num].nom}</h2>
      <p className="p-fab p-infos">
        <img
          className="fab-marker"
          src="https://img.icons8.com/color/48/000000/marker--v1.png"
          alt="logo"
        />
        {Json[props.num].infos}
      </p>
      <p className="p-fab p-form">{Json[props.num].formation}</p>
    </div>
  );
}

export default Info;
